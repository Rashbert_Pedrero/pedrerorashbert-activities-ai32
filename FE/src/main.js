import Vue from 'vue'
import dashboard from './components/pages/dashboard/dashboard.vue'

Vue.config.productionTip = false

import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'

new Vue({
  render: h => h(dashboard),
}).$mount('#app')
