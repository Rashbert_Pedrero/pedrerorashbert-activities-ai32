<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource
     * 
     *  @return \Illuminate\Http\Response
     */

    public function index()
    {
       return response()->json(Category::all());    
    }

    /**
     * Store a newly created resouces in storage 
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

     public function store(CategoryRequest $request)
     {
         $categories = new Category();
         $categories->category = $request->category;
         $validated = $request->validated();
        
         $categories->save();
         return response()->json($categories);
     }

     /**
      * Display the specified resource.
      * 
      * @param int $id
      * @return \Illuminate\Https\Response
      */

      public function show($id)
      {
          $categories = Category::find($id);
          return responses()->json($categories);
      }

      /**
       * Update the specified resource in storage.
       *
       * @param \Illuminate\Http\Request $request
       * @param int $id
       * @return \Illuminate\Http\Response
       */

       public function update(CategoryRequest $request, $id)
       {
           $categories = Category::find($id);
           $categories->category = $request->category;
           $validated = $request->validated();

           $categories->update();
           return response()->json($categories);
       }
}
