<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BorrowedBook;
use App\Models\ReturnedBook;
use App\Models\Book;
use App\Http\Requests\BorrowedRequest;

class BorrowedBookController extends Controller
{
    public function index()
    {
        $borrowedbooks = BorrowedBook::all();
        return response()->json([
            "message" => "LIST OF BORROWED BOOKS",
            "data" => $borrowedbooks
        ]);       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BorrowedRequest $request)
    {
        $borrowedbooks = new BorrowedBook();
        $borrowedbooks->copies = $request->copies;
        $borrowedbooks->book_id = $request->book_id;
        $borrowedbooks->patron_id = $request->patron_id;
        $validated = $request->validated();

        $book = Book::find($borrowedbooks->book_id);

        if($borrowedbooks->copies > $book->copies){
            return response()->json(
                ["message" => "THE BOOK BORROWED IS GREATER THAN BOOK COPIES"]);
            }
            else{

        $minusbookcopies = $book->copies - $borrowedbooks->copies;
        
        $borrowedbooks->save();
        $book->update(['copies' => $minusbookcopies]);//TO UPDATE THE COPIES OF BOOKS
        return response()->json(
               ["message" => "Updated Book Copies",
               "data" => $borrowedbooks, $book]);
    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $borrowedbooks = BorrowedBook::find($id);
        return response()->json($borrowedbooks);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BorrowedRequest $request, $id)
    {
        $borrowedbooks = BorrowedBook::find($id);
        $returnedbooks = new ReturnedBook();
        
        $returnedbooks->copies = $request->copies;
        $returnedbooks->book_id = $request->book_id;
        $returnedbooks->patron_id = $request->patron_id;
        $validated = $request->validated();

        $book = Book::find($returnedbooks->book_id);

        if($borrowedbooks->copies == $borrowedbooks->copies){
            $borrowedbooks->delete();
        }

        if($borrowedbooks->copies < $borrowedbooks->copies){
            return response()->json(
                ["message" => "THE RETURNED BOOK COPIES MUST NOT EXCEED ", $borrowedbooks->copies ]);
        }
        
        $updatedcopies = $book->copies + $returnedbooks->copies;
        $borrowedbookscopies =  $borrowedbooks->copies - $returnedbooks->copies;

        $returnedbooks->save();
        $borrowedbooks->update(['copies' => $borrowedbookscopies]);
        $book->update(['copies' => $updatedcopies]);
        return response()->json(
               ["message" => "THE BORROWED BOOK SUCCESFULLY RETURN"]);
    }

}