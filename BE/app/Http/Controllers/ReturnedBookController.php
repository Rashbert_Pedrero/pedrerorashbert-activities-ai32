<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ReturnedBook;
use App\Models\Book;

class ReturnedBookController extends Controller
{
    public function index()
    {
        $returnedbooks = ReturnedBook::all();
        return response()->json([
            "message" => "LIST OF BORROWED BOOKS",
            "data" => $returnedbooks
        ]);       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $returnedbooks = ReturnedBook::find($id);
        return response()->json($returnedbooks);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $returnedbooks = ReturnedBook::find($id);
        $returnedbooks->delete();
        return response()->json($returnedbooks);
    }
}
