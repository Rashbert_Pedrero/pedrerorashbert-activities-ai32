<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Patron;
use App\Http\Requests\PatronRequest;

class PatronController extends Controller
{
    /**
     * Display a listing of the resource
     * 
     *  @return \Illuminate\Http\Response
     */

     public function index()
     {
        return response()->json(Patron::all());
     }

     /**
      * Store a newly created resouces in storage 
      *
      * @param \Illuminate\Http\Request $request
      * @return \Illuminate\Http\Response
      */

      public function store(PatronRequest $request)
      {
          $patrons = new Patron();
          $patrons->first_name = $request->first_name;
          $patrons->middle_name = $request->middle_name;
          $patrons->last_name = $request->last_name;
          $patrons->email = $request->email;
          $validated = $request->validated();

          $patrons->save();
          return response()->json($patrons);
      }

      /**
       * Display the specified resource.
       * 
       * @param int $id
       * @return \Illuminate\Https\Response
       */

       public function show($id)
       {
           $patrons = Patron::find($id);
           return responses()->json($patrons);
       }

       /**
        * Update the specified resource in storage.
        *
        * @param \Illuminate\Http\Request $request
        * @param int $id
        * @return \Illuminate\Http\Response
        */

        public function update(PatronRequest $request, $id)
        {
            $patrons = Patron::where('id', $id)->firstOrFail();
            $patrons->update($request->validated());
            return response()->json(['message' => 'Patron updated.', 'patron' => $patrons]);
        }

        /**
         * Remove the specified resource from storage
         * 
         * @param int $id
         * @return \Illuminate\Http\Response
         */

         public function destroy($id)
         {
            try {
                $patrons = Patron::where('id', $id)->firstOrFail();
                $patrons->delete();
                
                return response()->json(['message' => 'Patron deleted.']);
            } catch (ModelNotFoundException $e) {
                return response()->json(['message' => 'Patron not found.'], 404);
    
            }
         }
}
