<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Http\Requests\BookRequest;


class BookController extends Controller
{
    /**
     * Display a listing of the resource
     * 
     *  @return \Illuminate\Http\Response
     */

    public function index()
    {
        return response()->json(Book::with(['category:id,category'])->get());
    }

    /**
     * Store a newly created resouces in storage 
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

     public function store(BookRequest $request)
     {
         $Book = new Book();
         $Book->name = $request->name;
         $Book->author = $request->author;
         $Book->copies = $request->copies;
         $Book->category_id = $request->category_id;

         $validated = $request->validated();
         $Book->save();
         return response()->json($Book);
     }

     /**
      * Display the specified resource.
      * 
      * @param int $id
      * @return \Illuminate\Https\Response
      */

      public function show($id)
      {
        try {
            return response()->json(Book::with(['category:id,category'])->where('id', $id)->firstOrFail());
        }catch(ModelNotFoundException $e) {
            return response()->json(['message' => 'Book not found'], 404);
        }
      }

      /**
       * Update the specified resource in storage.
       *
       * @param \Illuminate\Http\Request $request
       * @param int $id
       * @return \Illuminate\Http\Response
       */

       public function update(BookRequest $request, $id)
       {
        try {

            $books = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();
            $books->update($request->validated());
            return response()->json(['message' => 'Book updated.', 'book' => $books->with(['category:id,category'])->where('id', $book->id)->firstOrFail()]);

        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Book not found'], 404);
        }
       }

       /**
        * Remove the specified resource from storage
        * 
        * @param int $id
        * @return \Illuminate\Http\Response
        */

        public function destroy($id)
        {
            try {

                $books = Book::where('id', $id)->firstOrFail();
                $books->delete();
                
                return response()->json(['message' => 'Book deleted.']);
            } catch (ModelNotFoundException $e) {
                return response()->json(['message' => 'Book not found.'], 404);
    
            }
        }
}
