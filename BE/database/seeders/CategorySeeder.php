<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Model\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mul_rows= [
            [ 'category' => 'Romance'],
            [ 'category' => 'History'],
            [ 'category' => 'Geography'],
            [ 'category' => 'Fiction'],
            [ 'category' => 'Music']
        ];
        
            foreach ($mul_rows as $rows) {
            Category::create($rows);
                }
    }
}
