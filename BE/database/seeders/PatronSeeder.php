<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Factory as Faker;
use App\Models\Patron;

class PatronSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
            foreach(range(1,10) as $index){
                $firstName = $faker->FirstName;
                Patron::create([
                    'last_name' => $faker->LastName,
                    'first_name' => $firstName,
                    'middle_name' => $faker->FirstName,
                    'email' => $firstName.'@gmail.com',
                    ]);
            }
    }
}
