var ctx = document.getElementById('myBar').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['JAN', 'FEB', 'MARCH', 'APRIL', 'MAY', 'JUNE'],
        datasets: [{
            label: 'Borrowed Books',
            data: [25, 36, 29, 50, 22, 27],
            backgroundColor: [
                'rgba(26,188,156 ,1)',
                'rgba(26,188,156 ,1)',
                'rgba(26,188,156 ,1)',
                'rgba(26,188,156 ,1)',
                'rgba(26,188,156 ,1)',
                'rgba(26,188,156 ,1)',
                
            ],
            hoverBackgroundcolor: 'cyan',
        },{
            label: 'Returned Books',
            data: [32, 21, 17, 29, 33, 8],
            backgroundColor: [
                'rgba(230,126,34 ,1)',
                'rgba(230,126,34 ,1)',
                'rgba(230,126,34 ,1)',
                'rgba(230,126,34 ,1)',
                'rgba(230,126,34 ,1)',
                'rgba(230,126,34 ,1)',
                
            ],
            hoverBackgroundcolor: 'brown',
        
        }]
    },
    options: {
        tooltips:{
            mode: 'index',
        },
                 scales: {
                      xAxes: [{
                         stacked: true,
                        }],
                        yAxes: [{
                            stacked: true,
                        }]
                    }
                
            }
});


var pie = document.getElementById('myPie').getContext('2d');
var chart1 = new Chart(pie, {
    type: 'doughnut',
    data: {
        labels: ['Novel', 'Fiction', 'Biography', 'Horror'],
        datasets: [{
            label: '# of Votes',
            data: [7, 5, 9, 10],
            backgroundColor: [
                'rgba(26,188,156,1.0)',
                'rgba(230, 126, 34,1.0)',
                'rgba(52, 152, 219,1.0)',
                'rgba(241, 196, 15,1.0)'
                
            ],
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        } 
    }
});



